using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

public static class HashEncrypt
{
    //TODO: Use this function to store passwords Hashed in the Database instead of plain Text.
    //TODO: Iterate throughout the passwords in the Table to Hash them.
    public static String sha256_hash(String value)
    {
        Byte[] result = SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(value));

        return String.Join(String.Empty, result.Select(x => x.ToString("x2")));
    }
}