using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HospitalWebService.Models;
using Microsoft.AspNetCore.Authorization;

namespace HospitalWebService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize(Roles = "MEDICO, ENFERMEIRO, PACIENTE, ADMIN")]
    public class EspecialidadesController : ControllerBase
    {
        private readonly itdb1Context _context;

        public EspecialidadesController(itdb1Context context)
        {
            _context = context;
        }

        // GET: api/Especialidades
        [HttpGet]
        [Authorize(Roles = "MEDICO, ENFERMEIRO, ADMIN, PACIENTE")] 
        public async Task<ActionResult<IEnumerable<Especialidades>>> GetEspecialidades() // List all Especialidades
        {
            return await _context.Especialidades.ToListAsync(); 
        }

        // GET: api/Especialidades/id/5
        [HttpGet("id/{id}")]
        [Authorize(Roles = "MEDICO, ENFERMEIRO, ADMIN")] 
        public async Task<ActionResult<Especialidades>> GetEspecialidadesById(int id) // Get Especialidades by CodEspecialidade == id
        {
            var especialidades = await _context.Especialidades.FindAsync(id);

            if (especialidades == null)
            {
                Console.WriteLine("Nenhuma especialidade com o id: " + id);
                return NotFound();
            }

            return especialidades;
        }

        // GET: api/Especialidades/valor/300
        [HttpGet("valor/{valor}")]
        [Authorize(Roles = "MEDICO, ENFERMEIRO, ADMIN, PACIENTE")] 
        public async Task<ActionResult<IEnumerable<Especialidades>>> GetEspecialidadesByValor(decimal valor) // Get Especialidades by ValorConsulta == valor
        {
            var especialidades = await _context.Especialidades.Where(esp => esp.ValorConsulta == valor).ToListAsync();
            if (especialidades.Count() < 1)
            {
                Console.WriteLine("Nenhuma especialidade com o Valor: " + valor);
                return NotFound();
            }

            return especialidades;
        }

        // GET: api/Especialidades/nome/Neurologista
        [HttpGet("nome/{nome}")]
        [Authorize(Roles = "MEDICO, ENFERMEIRO, ADMIN, PACIENTE")] 
        public async Task<ActionResult<IEnumerable<Especialidades>>> GetEspecialidadesByNome(string nome) // Get Especialidades by Nome == nome
        {
            var especialidades = await _context.Especialidades.Where(esp => esp.Nome.Equals(nome)).ToListAsync();
            if (especialidades.Count() < 1)
            {
                Console.WriteLine("Nenhuma especialidade com o Nome: " + nome);
                return NotFound();
            }

            return especialidades;
        }

        // PUT: api/Especialidades/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> PutEspecialidades(int id, Especialidades especialidades)
        {
            if (id != especialidades.CodEspecialidade)
            {
                return BadRequest();
            }

            _context.Entry(especialidades).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EspecialidadesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Especialidades
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<Especialidades>> PostEspecialidades(Especialidades especialidades)
        {
            _context.Especialidades.Add(especialidades);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEspecialidades", new { id = especialidades.CodEspecialidade }, especialidades);
        }

        // DELETE: api/Especialidades/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<Especialidades>> DeleteEspecialidades(int id)
        {
            var especialidades = await _context.Especialidades.FindAsync(id);
            if (especialidades == null)
            {
                return NotFound();
            }

            _context.Especialidades.Remove(especialidades);
            await _context.SaveChangesAsync();

            return especialidades;
        }

        private bool EspecialidadesExists(int id)
        {
            return _context.Especialidades.Any(e => e.CodEspecialidade == id);
        }
    }
}
