using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HospitalWebService.Models;
using System.Text.RegularExpressions;
using HospitalWebService.DI;
using Microsoft.AspNetCore.Authorization;

namespace HospitalWebService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ConsultasController : ControllerBase
    {
        private readonly IFormatter _formatter;
        private readonly itdb1Context _context;
        private readonly IRoleChecker _checker;

        public ConsultasController(itdb1Context context, IFormatter formatter, IRoleChecker checker)
        {
            _context = context;
            _formatter = formatter;
            _checker = checker;
        }
        [HttpGet]
        [Route("Paciente/{cpf}")]
        [Authorize(Roles = "MEDICO, ENFERMEIRO, PACIENTE, ADMIN")]
        public async Task<ActionResult<IEnumerable<ConsultasDtos>>> GetConsultasByPaciente(string cpf, [FromQuery] DateTime? datainicial = null, [FromQuery] DateTime? datafinal = null)
        {
            var formattedCpf = _formatter.RemoveCharactersAndSpecials(cpf);
            var pacient = await _context.Pacientes.FindAsync(formattedCpf);
            if(User.IsInRole("PACIENTE")){
                if (!_checker.ValidateUserAuthorization(User, pacient.Cpf)){
                    return Unauthorized(new { Message = "Não é possível verificar as consutas de outro paciente."});
                }
            }
            if (pacient == null){
                return NotFound();
            } 
            if (datainicial == null && datafinal == null){
                return await _context.Consultas.Where(c => c.Cpf == cpf).Select(c => new ConsultasDtos{ Cpf = c.Cpf, Crm = c.Crm, NomeMedico = c.CrmNavigation.Nome, NomeEnfermeiro = c.CorenNavigation.Nome, DataConsulta = c.DataConsulta.Date, Coren = c.Coren}).ToListAsync(); 
            }
            else if(datainicial != null && datafinal == null) {
                return await _context.Consultas.Where(c => c.Cpf == cpf && c.DataConsulta >= datainicial).Select(c => new ConsultasDtos{ Cpf = c.Cpf, Crm = c.Crm, NomeMedico = c.CrmNavigation.Nome, NomeEnfermeiro = c.CorenNavigation.Nome, DataConsulta = c.DataConsulta.Date, Coren = c.Coren}).ToListAsync(); 
            }
            else if(datainicial == null && datafinal != null){
                return await _context.Consultas.Where(c => c.Cpf == cpf && c.DataConsulta <= datafinal).Select(c => new ConsultasDtos{ Cpf = c.Cpf, Crm = c.Crm, NomeMedico = c.CrmNavigation.Nome, NomeEnfermeiro = c.CorenNavigation.Nome, DataConsulta = c.DataConsulta.Date, Coren = c.Coren}).ToListAsync(); 
            }
            else{
                return await _context.Consultas.Where(c => c.Cpf == cpf && c.DataConsulta >= datainicial && c.DataConsulta <= datafinal).Select(c => new ConsultasDtos{ Cpf = c.Cpf, Crm = c.Crm, NomeMedico = c.CrmNavigation.Nome, NomeEnfermeiro = c.CorenNavigation.Nome, DataConsulta = c.DataConsulta.Date, Coren = c.Coren}).ToListAsync(); 
            }
        }

        [HttpGet]
        [Route("Medico/{crm}")]
        [Authorize(Roles = "MEDICO, ADMIN")]
        public async Task<ActionResult<IEnumerable<ConsultasDtos>>> GetConsultasByMedico(string crm, [FromQuery] DateTime? datainicial = null, [FromQuery] DateTime? datafinal = null)
        {
            var formattedCrm = _formatter.RemoveCharactersAndSpecials(crm);
            var doctor = await _context.Medicos.FindAsync(formattedCrm);
            if(User.IsInRole("MEDICO")){
                if (!_checker.ValidateDoctorAuthorization(User, doctor.Crm)){
                    return Unauthorized(new { Message = "Não é possível verificar as consutas de outro médico."});
            }
            }
            if (doctor == null){
                return NotFound();
            }
            if (datainicial == null && datafinal == null){
                return await _context.Consultas.Where(c => c.Crm == doctor.Crm).Select(c => new ConsultasDtos{ Cpf = c.Cpf, Crm = c.Crm, NomeMedico = c.CrmNavigation.Nome, NomeEnfermeiro = c.CorenNavigation.Nome, DataConsulta = c.DataConsulta.Date, Coren = c.Coren}).ToListAsync(); 
            }
            else if(datainicial != null && datafinal == null) {
                return await _context.Consultas.Where(c =>c.Crm == doctor.Crm && c.DataConsulta >= datainicial).Select(c => new ConsultasDtos{ Cpf = c.Cpf, Crm = c.Crm, NomeMedico = c.CrmNavigation.Nome, NomeEnfermeiro = c.CorenNavigation.Nome, DataConsulta = c.DataConsulta.Date, Coren = c.Coren}).ToListAsync(); 
            }
            else if(datainicial == null && datafinal != null){
                return await _context.Consultas.Where(c => c.Crm == doctor.Crm && c.DataConsulta <= datafinal).Select(c => new ConsultasDtos{ Cpf = c.Cpf, Crm = c.Crm, NomeMedico = c.CrmNavigation.Nome, NomeEnfermeiro = c.CorenNavigation.Nome, DataConsulta = c.DataConsulta.Date, Coren = c.Coren}).ToListAsync(); 
            }
            else{
                return await _context.Consultas.Where(c => c.Crm == doctor.Crm && c.DataConsulta >= datainicial && c.DataConsulta <= datafinal).Select(c => new ConsultasDtos{ Cpf = c.Cpf, Crm = c.Crm, NomeMedico = c.CrmNavigation.Nome, NomeEnfermeiro = c.CorenNavigation.Nome, DataConsulta = c.DataConsulta.Date, Coren = c.Coren}).ToListAsync(); 
            }
        }
        // GET: api/Consultas
        [HttpGet]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<IEnumerable<Consultas>>> GetConsultas()
        {
            return await _context.Consultas.ToListAsync();
        }

        // GET: api/Consultas/5
        [HttpGet("{id}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<Consultas>> GetConsultas(int id)
        {
            var consultas = await _context.Consultas.FindAsync(id);

            if (consultas == null)
            {
                return NotFound();
            }

            return consultas;
        }

        // PUT: api/Consultas/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> PutConsultas(int id, Consultas consultas)
        {
            if (id != consultas.CodConsultas)
            {
                return BadRequest();
            }

            _context.Entry(consultas).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ConsultasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Consultas
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<Consultas>> PostConsultas(Consultas consultas)
        {
            _context.Consultas.Add(consultas);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetConsultas", new { id = consultas.CodConsultas }, consultas);
        }

        // DELETE: api/Consultas/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<Consultas>> DeleteConsultas(int id)
        {
            var consultas = await _context.Consultas.FindAsync(id);
            if (consultas == null)
            {
                return NotFound();
            }

            _context.Consultas.Remove(consultas);
            await _context.SaveChangesAsync();

            return consultas;
        }

        private bool ConsultasExists(int id)
        {
            return _context.Consultas.Any(e => e.CodConsultas == id);
        }
    }
}
