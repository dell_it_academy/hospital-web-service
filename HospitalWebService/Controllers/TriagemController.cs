using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HospitalWebService.Models;
using Microsoft.AspNetCore.Authorization;

namespace HospitalWebService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize(Roles = "MEDICO, ENFERMEIRO, PACIENTE, ADMIN")]
    public class TriagemController : ControllerBase
    {
        private readonly itdb1Context _context;

        public TriagemController(itdb1Context context)
        {
            _context = context;
        }

        // GET: api/Triagem
        [HttpGet]   
        [Authorize(Roles = "MEDICO, ENFERMEIRO, ADMIN")]     
        public async Task<ActionResult<IEnumerable<Triagem>>> GetTriagem()
        {
            return await _context.Triagem.ToListAsync();
        }

        // GET: api/Triagem/id/5
        [HttpGet("id/{id}")]
        [Authorize(Roles = "MEDICO, ENFERMEIRO, ADMIN")] 
        public async Task<ActionResult<Triagem>> GetTriagemById(int id)
        {
            var triagem = await _context.Triagem.FindAsync(id);

            if (triagem == null)
            {
                return NotFound();
            }

            return triagem;
        }

        // GET: api/Triagem/prioridade/5
        [HttpGet("prioridade/{prioridade}")]
        [Authorize(Roles = "MEDICO, ENFERMEIRO, ADMIN")] 
        public async Task<ActionResult<IEnumerable<Triagem>>> GetTriagemByPrioridade(int prioridade)
        {
            var triagem = await _context.Triagem.Where(tri => tri.Prioridade == prioridade).ToListAsync();
            Console.WriteLine("buscando prioridade: " + prioridade);
            
            if (triagem.Count() < 1)
            {
                Console.WriteLine("Nenhuma triagem com a prioridade: " + prioridade);
                return NotFound();
            }

            return triagem;
        }

        // GET: api/Triagem/cpf/5
        [HttpGet("cpf/{cpf}")]
        [Authorize(Roles = "MEDICO, ENFERMEIRO, PACIENTE, ADMIN")]
        public async Task<ActionResult<IEnumerable<Triagem>>> GetTriagemByCpf(string cpf)
        {
            if(User.Identity.Name.Equals(cpf) || User.Identity.Name.Length == 14 || User.Identity.Name.Length == 7 || GetCurrentUserRole() == "ADMIN")
            {
                var triagem = await _context.Triagem.Where(tri => tri.Cpf.Equals(cpf)).ToListAsync();
                Console.WriteLine("buscando cpf: " + cpf);
                if (triagem.Count() < 1)
                {
                    Console.WriteLine("Nenhuma triagem com o cpf: " + cpf);
                    return NotFound();
                }

                return triagem;
            }
            else
            {
                Console.WriteLine("CPF consultado diferente do CPF logado");
                return NotFound(new { message = "CPF consultado diferente do CPF logado" });
            }

        }

        // GET: api/Triagem/coren/5
        [HttpGet("coren/{coren}")]
        [Authorize(Roles = "MEDICO, ENFERMEIRO, ADMIN")]
        public async Task<ActionResult<IEnumerable<Triagem>>> GetTriagemByCoren(string coren)
        {
            var triagem = await _context.Triagem.Where(tri => tri.Coren.Equals(coren)).ToListAsync();
            Console.WriteLine("buscando coren: " + coren);
            if (triagem.Count() < 1)
            {
                Console.WriteLine("Nenhuma triagem com o coren: " + coren);
                return NotFound();
            }

            return triagem;
        }

        // GET: api/Triagem/paciente/cpf
        [HttpGet("paciente/cpf/{cpf}")]
        [Authorize(Roles = "MEDICO, ENFERMEIRO, PACIENTE, ADMIN")]
        public async Task<ActionResult<IEnumerable<TriagemDtos>>> GetTriagemByCpfwPac(string cpf)
        {
            if(User.Identity.Name.Equals(cpf) || User.Identity.Name.Length == 14 || User.Identity.Name.Length == 7 || GetCurrentUserRole() == "ADMIN")
            {
                //return await _context.Medicos.Include(m => m.CodEspecialidadeNavigation).Where(m => m.CodEspecialidadeNavigation.Nome.ToUpper().Contains(especialidade.ToUpper())).Select(m => new MedicosDtos{ Nome = m.Nome, CRM = m.Crm, Especialidade = m.CodEspecialidadeNavigation.Nome}).ToListAsync();
                return await _context.Triagem.Include(t => t.CpfNavigation).Where(t => t.CpfNavigation.Cpf.Equals(cpf)).Select(t => new TriagemDtos{ Cod_Triagem = t.CodTriagem, CPF = t.Cpf, NomePaciente = t.CpfNavigation.Nome, Coren = t.Coren, NomeEnfermeiro = t.CorenNavigation.Nome, DescricaoPaciente = t.DescricaoPaciente, DataConsulta = t.DataConsulta, Prioridade = t.Prioridade}).ToListAsync();
            }
            else{
                return NotFound();
            }
        }

        // GET: api/Triagem/paciente/nome
        [HttpGet("paciente/nome/{nome}")]
        [Authorize(Roles = "MEDICO, ENFERMEIRO, ADMIN")]
        public async Task<ActionResult<IEnumerable<TriagemDtos>>> GetTriagemByNomewPac(string nome)
        {
            return await _context.Triagem.Include(t => t.CpfNavigation).Where(t => t.CpfNavigation.Nome.ToUpper().Contains(nome.ToUpper())).Select(t => new TriagemDtos{ Cod_Triagem = t.CodTriagem, CPF = t.Cpf, NomePaciente = t.CpfNavigation.Nome, Coren = t.Coren, NomeEnfermeiro = t.CorenNavigation.Nome, DescricaoPaciente = t.DescricaoPaciente, DataConsulta = t.DataConsulta, Prioridade = t.Prioridade}).ToListAsync();
        }

        // GET: api/Triagem/paciente/sexo
        [HttpGet("paciente/sexo/{sexo}")]
        [Authorize(Roles = "MEDICO, ENFERMEIRO, ADMIN")]
        public async Task<ActionResult<IEnumerable<TriagemDtos>>> GetTriagemBySexowPac(string sexo)
        {
            return await _context.Triagem.Include(t => t.CpfNavigation).Where(t => t.CpfNavigation.Sexo.ToUpper().Equals(sexo.ToUpper())).Select(t => new TriagemDtos{ Cod_Triagem = t.CodTriagem, CPF = t.Cpf, NomePaciente = t.CpfNavigation.Nome, Coren = t.Coren, NomeEnfermeiro = t.CorenNavigation.Nome, DescricaoPaciente = t.DescricaoPaciente, DataConsulta = t.DataConsulta, Prioridade = t.Prioridade}).ToListAsync();
        }

        // GET: api/Triagem/enfermeiro/nome
        [HttpGet("enfermeiro/nome/{nome}")]
        [Authorize(Roles = "MEDICO, ENFERMEIRO, ADMIN")]
        public async Task<ActionResult<IEnumerable<TriagemDtos>>> GetTriagemByNomewEnf(string nome)
        {
            return await _context.Triagem.Include(t => t.CorenNavigation).Where(t => t.CorenNavigation.Nome.ToUpper().Contains(nome.ToUpper())).Select(t => new TriagemDtos{ Cod_Triagem = t.CodTriagem, CPF = t.Cpf, NomePaciente = t.CpfNavigation.Nome, Coren = t.Coren, NomeEnfermeiro = t.CorenNavigation.Nome, DescricaoPaciente = t.DescricaoPaciente, DataConsulta = t.DataConsulta, Prioridade = t.Prioridade}).ToListAsync();
        }

        // GET: api/Triagem/enfermeiro/coren
        [HttpGet("enfermeiro/coren/{coren}")]
        [Authorize(Roles = "MEDICO, ENFERMEIRO, ADMIN")]
        public async Task<ActionResult<IEnumerable<TriagemDtos>>> GetTriagemByCorenwEnf(string coren)
        {
            //return await _context.Medicos.Include(m => m.CodEspecialidadeNavigation).Where(m => m.CodEspecialidadeNavigation.Nome.ToUpper().Contains(especialidade.ToUpper())).Select(m => new MedicosDtos{ Nome = m.Nome, CRM = m.Crm, Especialidade = m.CodEspecialidadeNavigation.Nome}).ToListAsync();
            return await _context.Triagem.Include(t => t.CorenNavigation).Where(t => t.CorenNavigation.Coren.Equals(coren)).Select(t => new TriagemDtos{ Cod_Triagem = t.CodTriagem, CPF = t.Cpf, NomePaciente = t.CpfNavigation.Nome, Coren = t.Coren, NomeEnfermeiro = t.CorenNavigation.Nome, DescricaoPaciente = t.DescricaoPaciente, DataConsulta = t.DataConsulta, Prioridade = t.Prioridade}).ToListAsync();
        }

        // PUT: api/Triagem/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> PutTriagem(int id, Triagem triagem)
        {
            if (id != triagem.CodTriagem)
            {
                return BadRequest();
            }

            _context.Entry(triagem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TriagemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Triagem
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<Triagem>> PostTriagem(Triagem triagem)
        {
            _context.Triagem.Add(triagem);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTriagem", new { id = triagem.CodTriagem }, triagem);
        }

        // DELETE: api/Triagem/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<Triagem>> DeleteTriagem(int id)
        {
            var triagem = await _context.Triagem.FindAsync(id);
            if (triagem == null)
            {
                return NotFound();
            }

            _context.Triagem.Remove(triagem);
            await _context.SaveChangesAsync();

            return triagem;
        }

        private bool TriagemExists(int id)
        {
            return _context.Triagem.Any(e => e.CodTriagem == id);
        }

        private bool PacientesExists(string id)
        {
            return _context.Pacientes.Any(e => e.Cpf == id);
        }

        private string GetCurrentUserRole() => User
                                .Identities
                                .FirstOrDefault(x => x.Name == User.Identity.Name)
                                .Claims.FirstOrDefault(x => x.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role")
                                .Value;

        private string GetCurentUserCode() => User.Identity.Name;

    }
}
