using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HospitalWebService.Models;

namespace HospitalWebService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class MedicosController : ControllerBase
    {
        private readonly itdb1Context _context;

        public MedicosController(itdb1Context context)
        {
            _context = context;
        }
        ///<summary>
        ///Use this method to search doctors by their speciality
        ///</summary>
        ///<returns>
        ///The list of doctors for the speciality selected
        ///</returns>
        ///<param name="especialidade"> Speciality's name</param>
        ///
        [HttpGet("Especialidade/{especialidade}")]
        public async Task<ActionResult<IEnumerable<MedicosDtos>>> GetMedicosByEspecialidade(string especialidade)
        {
            //TODO: Verify if its possible to pass special characters(ascii encoding) as parameter in url.
            return await _context.Medicos.Include(m => m.CodEspecialidadeNavigation).Where(m => m.CodEspecialidadeNavigation.Nome.ToUpper().Contains(especialidade.ToUpper())).Select(m => new MedicosDtos{ Nome = m.Nome, CRM = m.Crm, Especialidade = m.CodEspecialidadeNavigation.Nome}).ToListAsync();
        }

        // GET: api/Medicos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Medicos>>> GetMedicos()
        {
            return await _context.Medicos.ToListAsync();
        }

        // GET: api/Medicos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Medicos>> GetMedicos(string id)
        {
            var medicos = await _context.Medicos.FindAsync(id);

            if (medicos == null)
            {
                return NotFound();
            }

            return medicos;
        }

        // PUT: api/Medicos/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMedicos(string id, Medicos medicos)
        {
            if (id != medicos.Crm)
            {
                return BadRequest();
            }

            _context.Entry(medicos).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedicosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Medicos
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Medicos>> PostMedicos(Medicos medicos)
        {
            _context.Medicos.Add(medicos);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (MedicosExists(medicos.Crm))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetMedicos", new { id = medicos.Crm }, medicos);
        }

        // DELETE: api/Medicos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Medicos>> DeleteMedicos(string id)
        {
            var medicos = await _context.Medicos.FindAsync(id);
            if (medicos == null)
            {
                return NotFound();
            }

            _context.Medicos.Remove(medicos);
            await _context.SaveChangesAsync();

            return medicos;
        }

        private bool MedicosExists(string id)
        {
            return _context.Medicos.Any(e => e.Crm == id);
        }
    }
}
