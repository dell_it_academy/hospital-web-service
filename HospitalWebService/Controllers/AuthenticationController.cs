using System;
using System.Linq;
using System.Threading.Tasks;
using HospitalWebService.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HospitalWebService.Controllers
{
    [Route("account")]
    public class AuthenticationController : ControllerBase
    {
        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public async Task<ActionResult<dynamic>> Authenticate([FromBody] Usuarios model)
        {
            using (itdb1Context db = new itdb1Context())
            {
                var listUsuarios = db.Usuarios.ToList();

                var user = listUsuarios.Where(x => x.Usercode.ToLower() == model.Usercode.ToLower() &&
                                x.Password.ToLower() == model.Password.ToLower())
                                .FirstOrDefault();

                if (user == null)
                    return NotFound(new { message = "Usuário ou Senha Inválidos" });

                var token = TokenService.GenerateToken(user);

                user.Password = "";

                return new { user = user, token = token };
            }
        }


        //----------------------- Métodos de exemplo de úso de autenticação. Devem ser excluidos depois. ----------------

        [HttpGet]
        [Route("anonimo")]
        [AllowAnonymous]
        public string Anonymous() => "Anônimo";

        [HttpGet]
        [Route("authenticated")]
        [Authorize]
        public string Authenticated() => String.Format("Autenticado {0} - {1}",
                    User.Identity.Name,
                    User
                        .Identities
                        .FirstOrDefault(x => x.Name == User.Identity.Name)
                        .Claims.FirstOrDefault(x => x.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role")
                        .Value);

        [HttpGet]
        [Route("autorizados")]
        [Authorize(Roles = "ENFERMEIRO,MEDICO")]
        public string Authorized() => "Enfermeiro ou Medico";

        [HttpGet]
        [Route("pacientes")]
        [Authorize(Roles = "PACIENTE")]
        public string Pacients() => "Paciente";

        // ----------------------------------------------------------------------------------------------
    }
}