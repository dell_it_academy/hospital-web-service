using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HospitalWebService.Models;
using Microsoft.AspNetCore.Authorization;

namespace HospitalWebService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PacientesController : ControllerBase
    {
        private readonly itdb1Context _context;

        public PacientesController(itdb1Context context)
        {
            _context = context;
        }

        // GET: Pacientes
        [HttpGet]
        [Authorize(Roles = "ADMIN,ENFERMEIRO,MEDICO")]
        public async Task<ActionResult<IEnumerable<Pacientes>>> GetPacientes()
        {
            return await _context.Pacientes.ToListAsync();
        }

        /*// GET: api/Pacientes/id/5
        [HttpGet("id/{id}")]
        public async Task<ActionResult<Pacientes>> GetPacientesById(string id)
        {
            var pacientes = await _context.Pacientes.FindAsync(id);

            if (pacientes == null)
            {
                return NotFound();
            }

            return pacientes;
        }*/

        // GET: Pacientes/cpf/{cpf}
        [HttpGet("cpf/{cpf}")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<Pacientes>>> GetPacientesByCpf(string cpf)
        {
            if (GetCurrentUserRole() == "PACIENTE" && GetCurentUserCode() != cpf)
            {
                return Unauthorized();
            }

            var pacientes = await _context.Pacientes.Where(pac => pac.Cpf.Equals(cpf)).ToListAsync();

            if (pacientes == null)
            {
                return NotFound();
            }

            return pacientes;
        }

        // GET: Pacientes/sexo/{sexo}
        [HttpGet("sexo/{sexo}")]
        [Authorize(Roles = "ADMIN,ENFERMEIRO,MEDICO")]
        public async Task<ActionResult<IEnumerable<Pacientes>>> GetPacientesBySexo(string sexo)
        {
            var pacientes = await _context.Pacientes.Where(pac => pac.Sexo.Equals(sexo)).ToListAsync();

            if (pacientes == null)
            {
                return NotFound();
            }

            return pacientes;
        }

        // PUT: Pacientes/{id}
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize(Roles = "ADMIN,ENFERMEIRO,MEDICO")]
        public async Task<IActionResult> PutPacientes(string id, Pacientes pacientes)
        {
            if (id != pacientes.Cpf)
            {
                return BadRequest();
            }

            _context.Entry(pacientes).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PacientesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: Pacientes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Authorize(Roles = "ADMIN,ENFERMEIRO,MEDICO")]
        public async Task<ActionResult<Pacientes>> PostPacientes(Pacientes pacientes)
        {
            _context.Pacientes.Add(pacientes);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PacientesExists(pacientes.Cpf))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetPacientes", new { id = pacientes.Cpf }, pacientes);
        }

        // DELETE: Pacientes/{id}
        [HttpDelete("{id}")]
        [Authorize(Roles = "ADMIN,ENFERMEIRO,MEDICO")]
        public async Task<ActionResult<Pacientes>> DeletePacientes(string id)
        {
            var pacientes = await _context.Pacientes.FindAsync(id);
            if (pacientes == null)
            {
                return NotFound();
            }

            _context.Pacientes.Remove(pacientes);
            await _context.SaveChangesAsync();

            return pacientes;
        }

        private bool PacientesExists(string id)
        {
            return _context.Pacientes.Any(e => e.Cpf == id);
        }

        private string GetCurrentUserRole() => User
                                .Identities
                                .FirstOrDefault(x => x.Name == User.Identity.Name)
                                .Claims.FirstOrDefault(x => x.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role")
                                .Value;

        private string GetCurentUserCode() => User.Identity.Name;
    }
}
