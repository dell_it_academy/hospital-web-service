using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HospitalWebService.Models;
using Microsoft.AspNetCore.Authorization;

namespace HospitalWebService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class EnfermeirosController : ControllerBase
    {
        private readonly itdb1Context _context;

        public EnfermeirosController(itdb1Context context)
        {
            _context = context;
        }

        // GET: Enfermeiros
        [HttpGet]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<IEnumerable<Enfermeiros>>> GetEnfermeiros()
        {
            return await _context.Enfermeiros.ToListAsync();
        }

        /*// GET: Enfermeiros/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Enfermeiros>> GetEnfermeirosById(string id)
        {
            var enfermeiros = await _context.Enfermeiros.FindAsync(id);

            if (enfermeiros == null)
            {
                return NotFound();
            }

            return enfermeiros;
        }*/

        // GET: Enfermeiros/coren/{coren}
        [HttpGet("coren/{coren}")]
        [Authorize(Roles = "ADMIN,ENFERMEIRO")]
        public async Task<ActionResult<IEnumerable<Enfermeiros>>> GetEnfermeirosByCoren(string coren)
        {
            if (GetCurrentUserRole() == "PACIENTE" && GetCurentUserCode() != coren)
            {
                return Unauthorized();
            }

            var enfermeiros = await _context.Enfermeiros.Where(enf => enf.Coren.Equals(coren)).ToListAsync();

            if (enfermeiros == null)
            {
                return NotFound();
            }

            return enfermeiros;
        }

        // GET: Enfermeiros/nome/gerson
        [HttpGet("nome/{nome}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<IEnumerable<Enfermeiros>>> GetEnfermeirosByNome(string nome)
        {
            var enfermeiros = await _context.Enfermeiros.Where(enf => enf.Nome.Equals(nome)).ToListAsync();

            if (enfermeiros == null)
            {
                return NotFound();
            }

            return enfermeiros;
        }

        // PUT: Enfermeiros/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> PutEnfermeiros(string id, Enfermeiros enfermeiros)
        {
            if (id != enfermeiros.Coren)
            {
                return BadRequest();
            }

            _context.Entry(enfermeiros).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EnfermeirosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: Enfermeiros
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<Enfermeiros>> PostEnfermeiros(Enfermeiros enfermeiros)
        {
            _context.Enfermeiros.Add(enfermeiros);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (EnfermeirosExists(enfermeiros.Coren))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetEnfermeiros", new { id = enfermeiros.Coren }, enfermeiros);
        }

        // DELETE: Enfermeiros/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<Enfermeiros>> DeleteEnfermeiros(string id)
        {
            var enfermeiros = await _context.Enfermeiros.FindAsync(id);
            if (enfermeiros == null)
            {
                return NotFound();
            }

            _context.Enfermeiros.Remove(enfermeiros);
            await _context.SaveChangesAsync();

            return enfermeiros;
        }

        private bool EnfermeirosExists(string id)
        {
            return _context.Enfermeiros.Any(e => e.Coren == id);
        }

        private string GetCurrentUserRole() => User
                                .Identities
                                .FirstOrDefault(x => x.Name == User.Identity.Name)
                                .Claims.FirstOrDefault(x => x.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role")
                                .Value;

        private string GetCurentUserCode() => User.Identity.Name;
    }
}
