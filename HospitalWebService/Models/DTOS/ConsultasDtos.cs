using System;

namespace HospitalWebService.Models
{
    public class ConsultasDtos
    {
        public string Cpf {get; set;}
        public string Coren {get; set;}
        public string NomeEnfermeiro {get; set;}
        public DateTime DataConsulta {get;set;}
        public string Crm {get; set;}
        public string NomeMedico {get;set;}

    }
}