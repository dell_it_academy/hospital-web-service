namespace HospitalWebService.Models{
    
    public class MedicosDtos{
        public string Nome { get; set;}
        public string CRM { get; set; }
        public string Especialidade { get; set; }
    }
}