using System;

namespace HospitalWebService.Models{
    
    public class TriagemDtos{
        public int Cod_Triagem { get; set;}
        public string CPF { get; set; }
        public string NomePaciente { get; set; }
        public string Coren { get; set; }
        public string NomeEnfermeiro { get; set; }
        public string DescricaoPaciente { get; set; }
        public DateTime DataConsulta { get; set; }
        public int Prioridade { get; set; }
    }
}
