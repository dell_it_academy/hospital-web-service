using System;
using System.Collections.Generic;

namespace HospitalWebService.Models
{
    public partial class Usuarios
    {
        public string Usercode { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }
}
