namespace HospitalWebService.DI
{
    public interface IFormatter
    {
        string RemoveCharactersAndSpecials(string text);
    }
}