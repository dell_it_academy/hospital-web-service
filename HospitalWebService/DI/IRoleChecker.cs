using System.Security.Claims;
using System.Threading.Tasks;

namespace HospitalWebService.DI
{
    public interface IRoleChecker
    {
        public bool ValidateDoctorAuthorization(ClaimsPrincipal user, string crm);
        public bool ValidateNurseAuthorization(ClaimsPrincipal user, string coren);
        public bool ValidateUserAuthorization(ClaimsPrincipal user, string cpf);
    }
}