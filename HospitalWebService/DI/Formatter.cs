using System.Text.RegularExpressions;
namespace HospitalWebService.DI
{
    public class Formatter : IFormatter
    {
        public string RemoveCharactersAndSpecials(string text)
        {
            return Regex.Replace(text, @"^\d$", "");
        }
    }
}