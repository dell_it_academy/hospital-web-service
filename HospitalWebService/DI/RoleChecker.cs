using System.Security.Claims;
using System.Threading.Tasks;
using HospitalWebService.Models;
namespace HospitalWebService.DI
{
    public class RoleChecker : IRoleChecker
    {
        public bool ValidateUserAuthorization(ClaimsPrincipal user, string cpf)
        {
            return user.Identity.Name == cpf;
        }

        public bool ValidateNurseAuthorization(ClaimsPrincipal user, string coren)
        {
            return user.Identity.Name == coren;
        }

        public bool ValidateDoctorAuthorization(ClaimsPrincipal user, string crm)
        {
            return user.Identity.Name == crm;
        }
    }
}